<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Product;
use App\Category;

class CategoryController extends Controller
{
    public function createCategory(Request $request)
    {
      $validator = \Validator::make($request->all(), [
          'category_name' => 'required|max:75|unique:categories',
      ]);

      if ($validator->fails()) {
          return response()->json(['errors'=>$validator->errors()]);
      }

      if ($request->ajax()) {
        $category = new Category;
        $category->category_name = $request->category_name;
        $category->save();

        return Response()->json(['success' => '1' ,'category' => $category]);
      }
    }

    public function showCategory()
    {
      $categories = Category::orderBy('category_name','asc')->get();
      return response()->json($categories);
    }

    public function showAvailableCategory()
    {
      $categories = Category::orderBy('category_name','asc')->get();
      return view('admin-views/admin-frm/category-frm/available-category',compact('categories'));
    }

    public function updateCategoryName(Request $request, Category $category)
    {
      $this->validate($request, [
        'category_name' => 'required|max:75|unique:categories',
      ]);

      $category->category_name = $request->category_name;
      $category->update();
      return back()->with('message',"Category name has changed to \"".$request->category_name."\"");
    }

    public function deleteCategory(Category $category)
    {
      $products = Category::find($category->id)->products;
      foreach ($products as $key => $product) {
        if ($product->product_image != NULL) {
          Storage::delete('public/product-img/'.$product->product_image);
        }
      }
      $category->delete();
      return back()->with('message',"Category \"".$category->product_name."\" has been deleted as well related products");

    }
}
