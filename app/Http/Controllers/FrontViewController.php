<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class FrontViewController extends Controller
{
    public function showProduts()
    {
      $categories = Category::orderBy('category_name','asc')->get();
      $products = Product::orderBy('product_name','asc')->paginate(6);
      return view('front-views/shop',compact(['categories','products']));
    }

    public function productForCategory($category)
    {
      $categories = Category::orderBy('category_name','asc')->get();
      $products = Category::find($category)->products()->paginate(6);
      $name = Category::find($category);
      return view('front-views/category-product',compact(['categories','products','name']));
    }
}
