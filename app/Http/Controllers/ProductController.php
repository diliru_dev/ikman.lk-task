<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Category;
use App\Product;

class ProductController extends Controller
{
    public function showProductFrm()
    {
      $categories = Category::orderBy('category_name','asc')->get();
      return view('admin-views/admin-frm/product-frm/product-frm',compact('categories'));
    }

    public function createProduct(Request $request)
    {

        $this->validate($request, [
          'category' => 'required',
          'product_name' => 'required|max:75',
          'product_price' => 'required|max:12|regex:/^\d*(\.\d{1,2})?$/',
          'product_image' => 'required|image|max:2048',
        ]);

        if ($request->hasFile('product_image')) {
          $productImage = uniqid() .'.'.$request->product_image->extension();
        }

        $product = new Product;
        $product->category_id = $request->category;
        $product->product_name = $request->product_name;
        $product->product_price = $request->product_price;
        if ($request->hasFile('product_image')) {
          $product->product_image = $productImage;
        }
        $product->save();

        if ($request->hasFile('product_image')) {
          $image = $request->product_image;
          $imageName = $productImage;
          $image->storeAs('public/product-img',$imageName);
        }
        return back()->with('message',"Product \"".$request->product_name."\" has been created");
    }

    public function showAvailableProducts()
    {
      $products = Product::orderBy('product_name','asc')->get();
      return view('admin-views/admin-frm/product-frm/available-product-frm',compact('products'));
    }

    public function editProduct(Product $product)
    {
      $categories = Category::orderBy('category_name','asc')->get();
      return view('admin-views/admin-frm/product-frm/product-edit',compact(['product','categories']));
    }

    public function updateProduct(Request $request, Product $product)
    {
      $this->validate($request, [
        'category' => 'required',
        'product_name' => 'required|max:75',
        'product_price' => 'required|max:12|regex:/^\d*(\.\d{1,2})?$/',
        'product_image' => 'image|max:2048',
      ]);

      if ($request->hasFile('product_image') && $product->product_image != NULL) {
        Storage::delete('public/product-img/'.$product->product_image);
      }

      if ($request->hasFile('product_image')) {
        $productImage = uniqid() .'.'.$request->product_image->extension();
      }

      $product->category_id = $request->category;
      $product->product_name = $request->product_name;
      $product->product_price = $request->product_price;
      if ($request->hasFile('product_image')) {
        $product->product_image = $productImage;
      }
      $product->update();

      if ($request->hasFile('product_image')) {
        $image = $request->product_image;
        $imageName = $productImage;
        $image->storeAs('public/product-img',$imageName);
      }
      return back()->with('message',"Product \"".$request->product_name."\" has been updated");

    }

    public function deleteProduct(Product $product)
    {
      if ($product->product_image != NULL) {
        Storage::delete('public/product-img/'.$product->product_image);
      }
      $product->delete();
      return back()->with('message',"Product \"".$product->product_name."\" has been deleted");
    }
}
