<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    @yield('title')

    <link href="{{asset('admin/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/plugins/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">
    @yield('css')

</head>

<body class="">

    <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">ikman.lk</strong>
                            </span></span></a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href=""
                              onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                              Logout</a>
                              <form id="logout-form" action="" method="POST" style="display: none;">
                                  {{ csrf_field() }}
                              </form>
                            </li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>              
                <li>
                    <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Product</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{url('admin/product-frm')}}"><i class="glyphicon glyphicon-pencil"></i>Add Product</a></li>
                        <li><a href="{{url('available/products')}}"><i class="glyphicon glyphicon-list-alt"></i>Available Product</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li><a href=""
                  onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                  <i class="fa fa-sign-out"></i> Logout</a>
                  <form id="logout-form" action="" method="POST" style="display: none;">
                      {{ csrf_field() }}
                  </form>
                </li>
            </ul>

        </nav>
        </div>

            <div class="wrapper wrapper-content">
              @yield('content')
            </div>
            <div class="footer">
                <div>
                    <strong>Copyright</strong> ikman.lk &copy; 2018-2020
                </div>
            </div>

        </div>
        </div>

    <!-- Mainly scripts -->
    <script src="{{asset('admin/js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset('admin/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('admin/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{asset('admin/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{asset('admin/js/inspinia.js')}}"></script>
    <script src="{{asset('admin/js/plugins/pace/pace.min.js')}}"></script>

    <!-- Jasny -->
    <script src="{{asset('admin/js/plugins/jasny/jasny-bootstrap.min.js')}}"></script>

    @yield('script')
</body>
</html>
