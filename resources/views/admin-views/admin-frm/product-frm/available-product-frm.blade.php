@extends('admin-views.layouts.main')

@section('title')
  <title>Avilable Products</title>
@endsection

@section('css')
  <link href="{{asset('admin/css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
@endsection

@section('content')

  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      @if(session()->has('message'))
        <div class="alert alert-danger">
          {{ session()->get('message') }}
        </div>
      @endif
      <div class="col-lg-12">
        <div class="ibox float-e-margins">
          <div class="ibox-title">
            <h5>Avilable Products</h5>
            <div class="ibox-tools">
              <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
              </a>
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-wrench"></i>
              </a>
              <ul class="dropdown-menu dropdown-user">
                <li><a href="#">Config option 1</a>
                </li>
                <li><a href="#">Config option 2</a>
                </li>
              </ul>
              <a class="close-link">
                <i class="fa fa-times"></i>
              </a>
            </div>
          </div>
          <div class="ibox-content">

            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" >
                <thead>
                  <tr>
                    <th>Product Image</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Edit</th>
                    <th>Delete</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($products as $key => $product)
                    <tr class="gradeX">
                      <td class="center">
                        <img src="{{asset('storage/product-img/'.$product->product_image)}}" alt="Smiley face" height="100" width="100" class="img-thumbnail">
                      </td>
                      <td>{{$product->product_name}}</td>
                      <td>{{$product->product_price}}</td>
                      <td>
                        <a class="btn-xs btn-primary" href="{{route('edit.product',['product' => $product->id])}}">
                          <span class="glyphicon glyphicon-edit"></span>  Edit
                        </a>
                      </td>
                      <td>
                        <form method="post" action="{{route('delete.product',$product->id)}}">
                          {{csrf_field()}}
                          <button class="btn-xs btn-danger" onclick="return confirm('Pleace confirm to delete record?')" type="submit">
                            <span class="glyphicon glyphicon-remove"></span> Delete
                          </button>
                        </form>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>Product Image</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Edit</th>
                    <th>Delete</th>
                  </tr>
                </tfoot>
              </table>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('script')
  <script src="{{asset('admin/js/plugins/dataTables/datatables.min.js')}}"></script>
  <script>
  $(document).ready(function(){
    $('.dataTables-example').DataTable({
      pageLength: 5,
      responsive: true,
      dom: '<"html5buttons"B>lTfgitp',

    });

  });
  </script>
@endsection
