@extends('admin-views.layouts.main')

@section('title')
  <title>Admin|Edit|Product</title>
@endsection

@section('css')
  <link href="{{asset('admin/css/plugins/summernote/summernote.css')}}" rel="stylesheet">
  <link href="{{asset('admin/css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet">
  <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">
@endsection

@section('content')

  <div class="row">

    @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif

    <div class="col-sm-offset-1 col-lg-10">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Edit Product</h5>
        </div>
        <div class="ibox-content">
          <form action="{{route('update.product',$product->id)}}" method="post" class="form-horizontal" enctype="multipart/form-data" id="create_product">
            <div class="form-group">
              <div class="col-sm-4{{ $errors->has('category') ? ' has-error' : '' }}">
                <label for="category">Category</label>
                <select class="form-control form-control-sm" name="category" id="category_list">
                  @foreach ($categories as $key => $category)
                    @if ($category->id === $product->category_id)
                      <option value="{{$category->id}}"selected>{{$category->category_name}}</option>
                    @else
                      <option value="{{$category->id}}">{{$category->category_name}}</option>
                    @endif
                  @endforeach
                </select>
                @if ($errors->has('category'))
                  <span class="help-block">
                    <strong>{{ $errors->first('category') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-6{{ $errors->has('product_name') ? ' has-error' : '' }}">
                <label for="product_name">Product Name</label>
                <input type="text" class="form-control" name="product_name" value="{{ $product->product_name }}">
                @if ($errors->has('product_name'))
                  <span class="help-block">
                    <strong>{{ $errors->first('product_name') }}</strong>
                  </span>
                @endif
              </div>
              <div class="col-sm-6{{ $errors->has('product_price') ? ' has-error' : '' }}">
                <label for="product_price">Product Price</label>
                <input type="text" class="form-control" name="product_price" value="{{ $product->product_price }}">
                @if ($errors->has('product_price'))
                  <span class="help-block">
                    <strong>{{ $errors->first('product_price') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-6{{ $errors->has('product_image') ? ' has-error' : '' }}">
                <label for="product_image">Product Image</label><br>
                <img src="{{asset('storage/product-img/'.$product->product_image)}}" alt="Smiley face" height="150" width="150" class="img-thumbnail">
                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                  <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                  <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select Image</span><span class="fileinput-exists">Change</span><input type="file" name="product_image"></span>
                  <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                </div>
                @if ($errors->has('product_image'))
                  <span class="help-block">
                    <strong>{{ $errors->first('product_image') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            {{csrf_field()}}
            <div class="hr-line-dashed"></div>
            <div class="form-group">
              <div class="col-sm-11">
                <a class="btn btn-success pull-left" href="{{route('available.product')}}" id="update-status">Back</a>
                <button class="btn btn-primary pull-right" type="submit">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>


@endsection

@section('script')
<script src="{{asset('js/category.js')}}"></script>
@endsection
