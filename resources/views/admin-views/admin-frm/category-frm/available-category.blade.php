@extends('admin-views.layouts.main')
@section('content')

  <!-- Main content -->
  <section class="content">
    @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
    @endif
    <div class="row">
      <div class="col-xs-12">
        <div class="box">

          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Category Name</th>
                    <th>Edit Category Name</th>
                    <th>Delete</th>
                  </tr>
                </thead>
                <tbody id="user-info">
                  @foreach ($categories as $category)
                    <tr>
                      <td>{{$category->category_name}}</td>
                      <td>
                        <div class="col-md-12">
                          <form action="{{route('update.category',$category->id)}}" method="post" class="form-horizontal" id="category"><!--category form start-->
                            <div class="form-group">
                              <div class="col-sm-5">
                                <label for="category_name">Category</label>
                                <input type="text" class="form-control" name="category_name" required>
                              </div>
                              <br>
                              <div class="col-sm-3">
                                <button class="btn-sm btn-primary" onclick="return confirm('Do you want to change category name?')" type="submit">Update</button>
                              </div>
                              <br><br>
                            </div>
                            {{csrf_field()}}
                          </form>
                        </div>
                      </td>
                      <td>
                        <br>
                        <form method="post" action="{{route('delete.category',$category->id)}}">
                          {{csrf_field()}}
                          <button class="btn-xs btn-danger" onclick="return confirm('Pleace confirm to delete category it also delete related product.Do you want to continue?')" type="submit">
                            <span class="glyphicon glyphicon-remove"></span> Delete
                          </button>
                        </form>
                      </td>
                    </tr>
                  @endforeach
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection

@section('script')

  <script type="text/javascript">

  </script>

@endsection
