<div class="col-sm-3">
  <div class="left-sidebar">

    <div class="brands_products"><!--brands_products-->
      <h2>Category</h2>
      <div class="brands-name">
        <ul class="nav nav-pills nav-stacked">
          <li><a href="{{url('/')}}"> <span class="pull-right"></span>All Products</a></li>
          @foreach ($categories as $key => $category)
          <li><a href="{{route('category.product',$category->id)}}"> <span class="pull-right"></span>{{$category->category_name}}</a></li>
          @endforeach
        </ul>
      </div>
    </div><!--/brands_products-->

  </div>
</div>
