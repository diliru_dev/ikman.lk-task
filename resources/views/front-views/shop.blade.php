@extends('front-views/layouts/master')

@section('content')

@include('front-views/layouts/header')<!--This will include all header section-->

	<section>
		<div class="container">
			<div class="row">

      @include('front-views/layouts/left-slidebar')

				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">All Items Available</h2>
						@foreach ($products as $key => $product)
							<div class="col-sm-4">
								<div class="product-image-wrapper">
									<div class="single-products">
										<div class="productinfo text-center">
											<img src="{{asset('storage/product-img/'.$product->product_image)}}" alt="" />
											<h2>{{'Rs: '.$product->product_price}}</h2>
											<p>{{$product->product_name}}</p>
										</div>
									</div>
								</div>
							</div>
						@endforeach

						<ul class="pagination">
							<li class="active"><a href="">1</a></li>
							{!! $product->paginate() !!}
						</ul>
					</div><!--features_items-->
				</div>
			</div>
		</div>
	</section>

@endsection
