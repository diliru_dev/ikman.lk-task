<?php

Route::get('admin/product-frm','ProductController@showProductFrm');
Route::post('admin/create/category', 'CategoryController@createCategory')->name('create.category');
Route::get('show/category', 'CategoryController@showCategory');
Route::post('create/product', 'ProductController@createProduct')->name('create.product');
Route::get('available/products', 'ProductController@showAvailableProducts')->name('available.product');
Route::get('product/{product}/edit', 'ProductController@editProduct')->name('edit.product');
Route::post('product/{product}', 'ProductController@updateProduct')->name('update.product');
Route::post('delete/product/{product}', 'ProductController@deleteProduct')->name('delete.product');
Route::get('available/category', 'CategoryController@showAvailableCategory');
Route::post('update/category/{category}', 'CategoryController@updateCategoryName')->name('update.category');
Route::post('delete/category/{category}', 'CategoryController@deleteCategory')->name('delete.category');

Route::get('/', 'FrontViewController@showProduts');
Route::get('category/{category}/products', 'FrontViewController@productForCategory')->name('category.product');
