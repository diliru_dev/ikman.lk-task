$('#create_product').on('submit',function(e){
  e.preventDefault();
  var url  = $(this).attr('action');
  var post = $(this).attr('method');
  $.ajax({
    type : post,
    url : url,
    data : new FormData(this),
    contentType : false,
    processData :false,
    success:function(data){
      if(data.errors) {
        $( '#category_list_error' ).empty();
        $( '#product_name_error' ).empty();
        $( '#product_price_error' ).empty();
        $( '#product_image_error' ).empty();
        if(data.errors.category) {
          $( '#category_list_error' ).html( data.errors.category);
          $( '#product_name_error' ).html( data.errors.product_name);
          $( '#product_price_error' ).html( data.errors.product_price);
          $( '#product_image_error' ).html( data.errors.product_image);
        }
      }
      if (data.success)
      {
        $('#message-2').empty();
        $('#message-2').append('<div class="alert alert-success">Product '+data.product.product_name+' has been inserted</div>');
        $("#create_product").get(0).reset();
      }
    }

  })

});
