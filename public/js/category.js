$('#category').on('submit',function(e) {
  e.preventDefault();
  var data = $(this).serialize();
  var url  = $(this).attr('action');
  var post = $(this).attr('method');
  $( '#category-error' ).html( "" );
  $.ajax({
    type : post,
    url : url,
    data : data,
    dataTy : 'json',
    success:function(data)
    {
      if(data.errors) {
        if(data.errors.category_name) {
          $( '#category-error' ).html( data.errors.category_name[0] );
        }
      }
      if(data.success) {
        $('#message-1').empty();
        $('#message-1').append('<div class="alert alert-success">Category '+data.category.category_name+' has been inserted</div>');
        $.get('/show/category', function(data){
          $('#category_list').empty();
          $('#category_list').append('<option value="">Select Category</option>');
          $.each(data, function(index, categoryObj){
            $('#category_list').append('<option value="'+categoryObj.id+'">'+categoryObj.category_name+'</option>');
          });
        });
        $("#category").get(0).reset();
      }
    }
  })
});
